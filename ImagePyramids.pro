TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    src/main.cpp

#INCLUDEPATH += C:/opencv-2.4.6/opencv/build/include

#LIBS += -LC:\opencv-2.4.6\opencv\build\x86\vc10\lib \
#    -lopencv_core246 \
#    -lopencv_contrib246 \
#    -lopencv_highgui246 \
#    -lopencv_imgproc246 \
#    -lopencv_gpu246 \
#    -lopencv_features2d246 \
#    -lopencv_calib3d246 \
#    -lopencv_flann246 \
#    -lopencv_legacy246 \
#    -lopencv_ml246 \
#    -lopencv_video246 \
#    -lopencv_nonfree246

CONFIG += link_pkgconfig
PKGCONFIG += opencv
