#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#include <iostream>
#include <string>
using namespace std;
using namespace cv;

/**
 * @brief kernel A default kernel to be used by the Pyramid process.
 */
Mat kernel = (1.0f/(16.0f*16.0f)) * (Mat_<double>(5, 5) <<
              1, 4, 6, 4, 1,
              4, 16, 24, 16, 4,
              6, 24, 36, 24, 6,
              4, 16, 24, 16, 4,
              1, 4, 6, 4, 1);


/**
 * @brief The PyramidLevel class
 * Represents one level of a Laplacian Pyramid
 */
class PyramidLevel {
public:
    Mat image;
    Mat scaledUp;
    Mat scaledUpFiltered;
    Mat diff;

    PyramidLevel(Mat image = Mat()){
        this->image = image;
    }


    /**
     * @brief Fills the current level and return the next (with only image)
     * @return The next PyramidLevel
     */
    PyramidLevel* createLevel(){
        // Create and output the next level image
        Mat nextImage = constructLevel(image);

        // Show the "dithered"-kind of resulting image
        Mat scaledLevel = scaleLevelUp(nextImage);
        this->scaledUp = scaledLevel;

        // Now we can filter using the kernel (multiplied by 4 to compensate the black dots)
        Mat scaledLevelFiltered = scaledLevel.clone();
        filter2D(scaledLevel, scaledLevelFiltered, -1, kernel * 4.0 , Point(-1,-1), 0.0, BORDER_REPLICATE);
        this->scaledUpFiltered = scaledLevelFiltered;


        // Now, lets calculate the difference between original and the constructed image
        Mat diffm = Mat::zeros(image.cols, image.rows, CV_8UC1);
        diffm = (image - scaledLevelFiltered) + 128;
        this->diff = diffm;


        return new PyramidLevel(nextImage);
    }


    /**
     * @brief constructLevel Constructs a image based on the image of the previous level
     * @param previousLevel Image of the previous level
     * @return The calculated image
     */
    Mat constructLevel(Mat previousLevel){
        // Creates a copy of the image and applies a 2d filter with the kernel
        Mat dst = previousLevel.clone();
        filter2D(previousLevel, dst, -1, kernel, Point(-1,-1), 0.0, BORDER_REPLICATE);

        // Setup the next level - size hardcoded
        Mat nextLevel = Mat(previousLevel.cols/2, previousLevel.rows/2, CV_8UC1);

        // Calculate the next level based on the article.
        // Each pixel is composed of the odd rows and cols of the input image
        Mat A = dst; //Alias
        for(int i = 0; i < nextLevel.rows; i++){
            for(int j = 0; j < nextLevel.cols; j++){
                int idxl = i * 2 + 1;
                int idyl = j * 2 + 1;
                int value = A.data[A.step[0]*idxl + A.step[1]*idyl];

                nextLevel.data[nextLevel.step[0]*i + nextLevel.step[1]*j] = value;
            }
        }

        return nextLevel;
    }


    /**
     * @brief scaleLevelUp Scales a image based on the same process we use to construct it.
     * Instead of just scaling up, we fill every other pixel in the cols and rows
     * @param level Image to be scaledUp
     * @return The scaled up image
     */
    static Mat scaleLevelUp(Mat level){
        // Now we want to reconstruct the original image, but using the same
        // process we used to make the level (instead of just scaling up)
        Mat scaledLevel = Mat::zeros(level.cols*2, level.rows*2, CV_8UC1);

        for(int i = 0; i < level.rows; i++){
            for(int j = 0; j < level.cols; j++){
                int idxl = i * 2 + 1;
                int idyl = j * 2 + 1;
                int value = level.data[level.step[0]*i + level.step[1]*j];

                scaledLevel.data[scaledLevel.step[0]*idxl + scaledLevel.step[1]*idyl] = value;
            }
        }

        return scaledLevel;
    }


    /**
     * @brief filterScaledLevel Filters the image using the same kernel used to construct the level
     * @param scaledLevel The scaled up level unfiltered
     * @return The filtered image
     */
    static Mat filterScaledLevel(Mat scaledLevel){
        Mat scaledLevelFiltered = scaledLevel.clone();
        filter2D(scaledLevel, scaledLevelFiltered, -1, kernel * 4.0 , Point(-1,-1), 0.0, BORDER_REPLICATE);
        return scaledLevelFiltered;
    }


    /**
     * @brief scaleLevelUpAndFilter Just a convenience function get the scaled and filtered image
     * @param level The image of the current level
     * @return The scaled and filtered image
     */
    static Mat scaleLevelUpAndFilter(Mat level){
        return filterScaledLevel(scaleLevelUp(level));
    }

    /**
     * @brief toPath Converts a name to path
     * @param name The name part (without path)
     * @param savepath The desired path
     * @return The converted string representing a path
     */
    string toPath(const string &name, const string &savepath = "save/"){
        string ext = name.substr(name.length()-4);
        string nname = "" + name;
        if(ext.compare(".jpg") != 0 && ext.compare(".png") != 0){
            nname = nname.append(".png");
        }

        return savepath + nname;
    }


    /**
     * @brief output Outputs the PyramidLevel images by showing or/and saving to disk.
     * @param name Desired suffix name
     * @param show Whether the information should be shown on screen
     * @param save Whether the information should be saved on disk
     * @param savepath The save path (if any)
     */
    void output(const string &name, bool show, bool save, const string &savepath = "save/"){
        if(show) {
            imshow(name + "-image", image);
            imshow(name + "-scaled-up", scaledUp);
            imshow(name + "-scaled-up-filtered", scaledUpFiltered);
            imshow(name + "-diff", diff);
        }
        if(save) {
            if(!image.empty()) imwrite(toPath(name + "-image", savepath), image);
            if(!scaledUp.empty()) imwrite(toPath(name + "-scaled-up", savepath), scaledUp);
            if(!scaledUpFiltered.empty()) imwrite(toPath(name + "-scaled-up-filtered", savepath), scaledUpFiltered);
            if(!diff.empty()) imwrite(toPath(name + "-diff", savepath), diff);
        }
    }
};




/**
 * @brief A Laplacian Pyramid
 */
class ImagePyramid {
public:

    /**
     * @brief create Creates the pyramid
     * @param img desired image
     */
    void create(Mat img){
        PyramidLevel* rootLevel = new PyramidLevel(img);
        pyramid.push_back(rootLevel);

        while(min(pyramid.back()->image.cols, pyramid.back()->image.rows) >= 2){
            pyramid.push_back(pyramid.back()->createLevel());
            cout << "Creating Level..." << endl;
        }

        //pyramid.back()->output("last-level", false, true);
    }


    /**
     * @brief reconstructImage
     * We simulate the reconstructing, considering we can only
     * access the diffs images and the value (only pixel) of the
     * last level of the piramid.
     * @return The reconstructed image
     */
    Mat reconstructImage(){
        Mat curImage = pyramid[pyramid.size()-1]->image;
        cout << pyramid[0]->image.cols << endl;
        for(int i = pyramid.size()-1; i > 0 ; i--){
            Mat scaledUp = PyramidLevel::scaleLevelUpAndFilter(curImage);
            curImage = (scaledUp + pyramid[i-1]->diff) - 128;
        }

        return curImage;
    }


    /**
     * @brief output Outputs all the levels to a directory
     * @param path Existing directory to output the files
     */
    void output(const string &path){
        for(int i = 0; i < pyramid.size(); i++){
            stringstream ss;
            ss << "level" << i;
            pyramid[i]->output(ss.str(), false, true, path);
        }
    }


    vector<PyramidLevel*> pyramid;
};






int main(int argc, char* argv[]){
    // Loads the desired image
    Mat img = imread("lena.png", CV_LOAD_IMAGE_GRAYSCALE);
    //output("image.jpg", img, show, save);

    // Constructs the pyramid
    ImagePyramid pyramid;
    pyramid.create(img);
    //pyramid.output("save/levels");

    // Reconstructs the image based only on the diffs and level0 image
    Mat recImage = pyramid.reconstructImage();
    imshow("Reconstructed.png", recImage);

    // Unused loop
    char key = 0;
    while(key != 27){
        key = cvWaitKey(30);
    }

    return 0;
}
